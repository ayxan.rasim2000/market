
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from .managers import CustomUserManager
# Create your models here.


# Create your models here.

class Company(models.Model):
   
    name= models.CharField(max_length=50)
    user = models.ForeignKey('CustomUser',on_delete=models.CASCADE, related_name='posts')

class Product(models.Model):
   company= models.ForeignKey('Company', on_delete=models.CASCADE)
   price= models.FloatField()
   detail= models.CharField(max_length= 250)
   order_number= models.IntegerField()


class CustomUser(AbstractUser):
    username = None
    email= models.EmailField(verbose_name="email", max_length=60,unique=True)
    company= models.CharField(max_length=30, unique=False)
    date_joined= models.DateTimeField(verbose_name="date_joined", auto_now= True)
    last_login= models.DateTimeField(verbose_name="last_login", auto_now=True)
    is_admin= models.BooleanField(default=False)
    is_active= models.BooleanField(default=True)
    is_staff= models.BooleanField(default=False)
    is_superuser= models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()
    def __str__(self):
        return self.company
